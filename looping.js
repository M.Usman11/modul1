//Soal 1 - while
let i = 0;
while(i <= 21){
    if(i === 0){
        console.log('LOOPING PERTAMA');
    } else if(i % 2 === 0){
        console.log(`${i} - I love coding`);
    } else if(i > 20){
        let j = i;
        while(j > 0){
            if(j > 20){
                console.log('LOOPING KEDUA');
            } else if(j % 2 === 0){
                console.log(`${j} - I will become a mobile developer`);
            }
            j--;
        }
    }
    i++;
}

console.log('=======================');
//Soal no 2 - for
for(let angka = 1; angka <= 20; angka++){
  if (angka % 3 === 0 && angka % 2 === 1) {
    console.log(angka + ' - I Love Coding');
  } else if (angka % 2 ===1) {
    console.log(angka + ' - Santai');
  } else {
    console.log(angka + ' - Berkualitas');
  }
}

console.log('=======================');
//Soal 3 - Persegi panjang #
for(let i = 0; i < 4; i++){
  let hasil = '';
  for(let j = 0; j < 8; j++){
      hasil += "#";
  }
  console.log(hasil);
}

console.log('=======================');
//Soal 4 - Membuat tangga
for(let i = 1; i <= 7; i++){
  let hasil = "";
  for(let j = 1; j <= i; j++){
      hasil += "#";
  }
  console.log(hasil);
}

console.log('=======================');
//Soal 5 - Papan catur
for(let i = 1; i <= 8; i++){
  let hasil = "";
  for(let j = 1; j <= 4; j++){
      if(i % 2 === 1){
          hasil += " #";
      } else {
          hasil += "# ";
      }
  }
  console.log(hasil);
}
